package transform;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import exceptions.NotValidException;

public class Entry {

    private static final Logger _log = LogManager.getLogger();

    private String identifier = null;
    private Cuisine myCuisine = null;
    private Grade myGrade = null;
    private Restaurant myRestaurant = null;

    public Entry(String input) throws Exception {
        String splits[] = splitter(input);
        DateFormat dformat = new SimpleDateFormat("MM/dd/yyyy");

        identifier = splits[0];

        myCuisine = new Cuisine(splits[7]);

        if ( ((null == splits[13]) || ("".equals(splits[13]) ))     ||
             ((null == splits[14]) || ("".equals(splits[14]) ))     ||
             ((null == splits[15]) || ("".equals(splits[15]) ))     ){
            // no grade!
            myGrade = null;
            // set validity to false
            throw new NotValidException("No grade available");
        }
        else {
            try {
                myGrade = new Grade(Integer.valueOf(splits[13]).intValue(),
                                    splits[14],
                                    dformat.parse(splits[15])
                                    );
            } catch (NumberFormatException e) {
                display(splits);
                throw new Exception("Grade causing NumberFormat issue for Identifier: " + identifier);
            } catch (ParseException e) {
                display(splits);
                throw new Exception("Grade causing ParseException for Identifier: " + identifier);
            }
        }

        myRestaurant = new Restaurant(splits[1]);
    }

    public String getIdentifier() {
        return identifier;
    }

    public Cuisine getCuisine() {
        return myCuisine;
    }

    public Grade getGrade() {
        return myGrade;
    }

    public Restaurant getRestaurant() {
        return myRestaurant;
    }

    private String[] splitter(String test) {
        String splits[] = new String[20];

        String quoteSplits[] = test.split("\"");
        int current = 0;
        for (int i = 0 ; i < quoteSplits.length ; i++) {
            if ((i % 2) == 0) {
                // regular
                if (quoteSplits[i].startsWith(",")) {
                    quoteSplits[i] = quoteSplits[i].substring(1, quoteSplits[i].length());
                }
                if (quoteSplits[i].endsWith(",")) {
                    quoteSplits[i] = quoteSplits[i].substring(0, quoteSplits[i].length()-1);
                }
                String midSplit[] = quoteSplits[i].split(",");
                for (String s : midSplit) {
                    splits[current] = s.trim();
                    current++;
                }
            }
            else {
                // quoted
                splits[current] = quoteSplits[i].trim();
                current++;
            }
        }

        return splits;
    }
    
    private void display(String splits[]) {
        for (int i = 0 ; i < splits.length ; i++) {
            _log.debug("i=" + i + " val=" + splits[i]);
        }
    }

}
