CREATE TABLE IF NOT EXISTS Address
(
    AddressID   int             NOT NULL,
    Boro        varchar(255),
    Building    varchar(255),
    Street      varchar(255),
    Zipcode     int,
    PRIMARY KEY (AddressID)
);

CREATE TABLE IF NOT EXISTS Contact
(
    ContactID   int             NOT NULL,
    Phone       int,
    PRIMARY KEY (ContactID)
);

CREATE TABLE IF NOT EXISTS Cuisine
(
    CuisineID   int             NOT NULL,
    Name        varchar(255),
    PRIMARY KEY (CuisineID)
);

CREATE TABLE IF NOT EXISTS Restaurant
(
    ID          int             NOT NULL,
    Name        varchar(255),
    AddressID   int,
    ContactID   int,
    PRIMARY KEY (ID),
    FOREIGN KEY (AddressID) REFERENCES Address(AddressID),
    FOREIGN KEY (ContactID) REFERENCES Contact(ContactID),
);

CREATE TABLE IF NOT EXISTS Violation
(
    ViolationID             int         NOT NULL,
    ViolationCode           varchar(4),
    ViolationDescription    varchar(255),
    PRIMARY KEY (ViolationID)
);

CREATE TABLE IF NOT EXISTS Grade
(
    ID          int         NOT NULL,
    Score       int,
    Grade       varchar(2),
    GradeDate   DATE,
    PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS Inspection
(
    InspectionID        int         NOT NULL,
    InspectionDate      DATE,
    RecordDate          DATE,
    ViolationID         int,
    PRIMARY KEY (InpsectionID),
    FOREIGN KEY (ViolationID) REFERENCES Violation(ViolationID)
);

CREATE TABLE IF NOT EXISTS Restaurant_Cuisine
(
    RestaurantID    int,
    CuisineID       int
    FOREIGN KEY (RestaurantID) REFERENCES Restaurant(ID),
    FOREIGN KEY (CuisineID) REFERENCES Cuisine(CuisineID),
);