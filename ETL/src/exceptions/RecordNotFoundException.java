package exceptions;

@SuppressWarnings("serial")
public class RecordNotFoundException extends Exception {

    public RecordNotFoundException(String msg) {
        super(msg);
    }
}
