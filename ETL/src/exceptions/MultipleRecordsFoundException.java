package exceptions;

@SuppressWarnings("serial")
public class MultipleRecordsFoundException extends Exception {

    public MultipleRecordsFoundException(String msg) {
        super(msg);
    }

}
