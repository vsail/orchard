package exceptions;

@SuppressWarnings("serial")
public class NotValidException extends Exception {

    public NotValidException(String msg) {
        super(msg);
    }

}
