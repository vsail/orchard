package main;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import exceptions.NotValidException;
import transform.Entry;

public class Transformer implements Runnable {

    private static final Logger _log = LogManager.getLogger();

    private int countTotal = 0;
    private int countIgnored = 0;
    private int countFailed = 0;

    private LinkedBlockingQueue<String> readFrom = null;
    private LinkedBlockingQueue<Entry> loadTo = null;

    public Transformer( LinkedBlockingQueue<String> readFrom,
                        LinkedBlockingQueue<Entry> loadTo) {
        this.readFrom = readFrom;
        this.loadTo = loadTo;
    }

    @Override
    public void run() {
        boolean loop = true;
        while (loop) {
            try {
                String nextLine = readFrom.poll(2, TimeUnit.MINUTES);
                countTotal++;
                if (null != nextLine) {
                    Entry anEntry = new Entry(nextLine);
                    loadTo.offer(anEntry);
                }
                else {
                    loop = false;
                }
            } catch (InterruptedException e) {
                loop = false;
                _log.info("Interrupted : " + e.getMessage());
            } catch (NotValidException e) {
                countIgnored++;
            } catch (Exception e) {
                _log.error("Error: " + e.getMessage());
                countFailed++;
            }
        }

        // close
        shutdown();
        _log.info("Total: " + countTotal);
        _log.info("Ignored: " + countIgnored);
        _log.info("Failed: " + countFailed);
    }

    private void shutdown() {
        // do something
        // nothing?
    }
}
