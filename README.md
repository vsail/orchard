# README #

This repo is for the orchard's assignment. Read in information from a file and load it into database.

JAVA is used throughout.
DB is provided by db4free.net and uses mysql.


### How do I get set up? ###

Use the run.sh in ETL project to run the code.
Inside the run.sh, there's a file name (currently: Restaurant.csv), which is what it reads from.
DB values are all currently hard-coded.


ETL:
----
Extractor - responsible for reading values from passed in files.

Transformer - responsible for using the read in line and putting it into appropriate objects.

Loader - responsible for taking those created objects and putting them into db.

Main - kicks off the application. Currently creates 1 extractor for every input file, 10 transformer threads and 30 loader threads (30 db connections), and a batch processing of 100 at a time. Can be changed in main to shorten/increase the time it takes to load entire file into DB.


orchardweb:
-----------
This was going to be an api-server from where you cuold get values out of the db.
Unfortunately, its not currently working as I ran into issues trying to get Heroku to use the mysql database.
As I have never used herou/maven setup before, the code is basically the 'hello world' sample, but with one new query addition. I was going to test it out that it works and then clean it up and make it "mine" so to speak. But, since I didn't get the DB working, I basically hit a wall.