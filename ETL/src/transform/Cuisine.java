package transform;

public class Cuisine {

    private String name;

    public Cuisine(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
