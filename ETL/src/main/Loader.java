package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import transform.Cuisine;
import transform.Entry;
import transform.Grade;
import transform.Restaurant;

public class Loader implements Runnable {

    private static final Logger _log = LogManager.getLogger();

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://db4free.net/vsailorchard";

    private static final String USER = "vsailorchard";
    private static final String PASS = "vivek2orchard";

    private static final String SELECT_R = "SELECT ID FROM Restaurant WHERE Name = ?";
    private static final String SELECT_C = "SELECT CuisineID FROM Cuisine WHERE Name = (?)";
    private static final String SELECT_G = "SELECT ID FROM Grade WHERE Score = ? AND " +
                                           "GRADE = ? AND GradeDate = ?";

    private static final String INSERT_RG = "INSERT INTO Restaurant_Grade VALUES (" +
                                            "(" + SELECT_R + "),(" + SELECT_G +"))";
    private static final String INSERT_RC = "INSERT INTO Restaurant_Cuisine VALUES (" +
                                            "(" + SELECT_R + "),(" + SELECT_C +"))";
    private static final String INSERT_R = "INSERT INTO Restaurant (Name) VALUES (?)";
    private static final String INSERT_G = "INSERT INTO Grade (Score, Grade, GradeDate) VALUES (?, ?, ?)";
    private static final String INSERT_C = "INSERT INTO Cuisine (Name) VALUES (?)";

    private static final int MAX_TIME = 40;
    private static final TimeUnit MAX_UNIT = TimeUnit.SECONDS;
           
    private Connection conn = null;
    private PreparedStatement ps_rg = null;
    private PreparedStatement ps_rc = null;
    private PreparedStatement ps_r = null;
    private PreparedStatement ps_c = null;
    private PreparedStatement ps_g = null;
    private PreparedStatement ps_r_s = null;
    private PreparedStatement ps_c_s = null;
    private PreparedStatement ps_g_s = null;

    private LinkedBlockingQueue<Entry> loadFrom = null;

    private boolean ready = false;

    private static HashMap<String, Cuisine> cMap = null;
    private static HashMap<String, Restaurant> rMap = null;

    private int count = 0;
    private int timesRun = 0;
    private int maxBatchSize = 0;

    public Loader(LinkedBlockingQueue<Entry> loadFrom, int maxSize) {
        this.loadFrom = loadFrom;
        maxBatchSize = maxSize;

        try {
            Class.forName(JDBC_DRIVER).newInstance();

            _log.info("Connecting to database");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            ps_rg = conn.prepareStatement(INSERT_RG);
            ps_rc = conn.prepareStatement(INSERT_RC);
            ps_r = conn.prepareStatement(INSERT_R);
            ps_g = conn.prepareStatement(INSERT_G);
            ps_c = conn.prepareStatement(INSERT_C);
            ps_r_s = conn.prepareStatement(SELECT_R);
            ps_g_s = conn.prepareStatement(SELECT_G);
            ps_c_s = conn.prepareStatement(SELECT_C);

            cMap = new HashMap<String, Cuisine>();
            rMap = new HashMap<String, Restaurant>();

            ready = true;
        } catch (ClassNotFoundException e) {
            _log.error("Class not found: " + e.getMessage());
        } catch (SQLException e) {
            _log.error("SQL Exception when getting connection: " + e.getMessage());
        } catch (InstantiationException e) {
            _log.error("Instantiation Error: " + e.getMessage());
        } catch (IllegalAccessException e) {
            _log.error("Illegal Access Error: " + e.getMessage());
        }
    }

    public boolean isReady() {
        return ready;
    }

    @Override
    public void run() {
        boolean loop = true;
        while (loop) {
            try {
                // get entry
                Entry anEntry = loadFrom.poll(MAX_TIME, MAX_UNIT);
                if (null != anEntry) {
                    // load values into db
                    //storeToDB(anEntry);
                    storeToMap(anEntry);
                }
                else {
                    if (count > 0) {
                        executeAll();
                    }
                    else {
                        loop = false;
                    }
                }
            } catch (InterruptedException e) {
                loop = false;
                _log.info("Interrupted: " + e.getMessage());
            } catch (SQLException e) {
                //_log.error("SQLException: " + e.getMessage());
            }
        }

        // exit
        try {
            shutdown();
        } catch (SQLException e) {
            _log.error("Error during closing");
        }
    }

    private void storeToMap(Entry toStore) throws SQLException {
        Cuisine c = toStore.getCuisine();
        Restaurant r = toStore.getRestaurant();
        Grade g = toStore.getGrade();

        // cuisine
        try {
            if (!cMap.containsKey(c.getName())) {
                cMap.put(c.getName(), c);
                ps_c.setString(1, c.getName());
                ps_c.addBatch();
            }
        } catch (SQLException e) {
            throw new SQLException("Cuisine Batch: " + e.getMessage());
        }

        // restaurant
        try {
            if (!rMap.containsKey(r.getName())) {
                rMap.put(r.getName(), r);
                ps_r.setString(1, r.getName());
                ps_r.addBatch();
            }
        } catch (SQLException e) {
            throw new SQLException("Restaurant Batch: " + e.getMessage());
        }

        // grade
        try {
            ps_g.setInt(1, g.getScore());
            ps_g.setString(2, g.getGrade());
            ps_g.setDate(3, new java.sql.Date(g.getDate().getTime()));
            ps_g.addBatch();
        } catch (SQLException e) {
            throw new SQLException("Grade Batch: " + e.getMessage());
        }

        // rest-cuisine
        try {
            ps_rc.setString(1, r.getName());
            ps_rc.setString(2, c.getName());
            ps_rc.addBatch();
        } catch (SQLException e) {
            throw new SQLException("Rest-Cuisine Batch: " + e.getMessage());
        }

        // rest-grade
        try {
            ps_rg.setString(1, r.getName());
            ps_rg.setInt(2, g.getScore());
            ps_rg.setString(3, g.getGrade());
            ps_rg.setDate(4, new java.sql.Date(g.getDate().getTime()));
            ps_rg.addBatch();
        } catch (SQLException e) {
            throw new SQLException("Rest-grade Batch: " + e.getMessage());
        }

        count++;

        if ((count % maxBatchSize) == 0) {
            executeAll();
            timesRun++;
        }
    }

    private void executeAll() throws SQLException {
        _log.info("Executing batch at count: " + ((timesRun*maxBatchSize) + count) );

        // Cuisine
        try {
            ps_c.executeBatch();
        } catch (SQLException e) {
            throw new SQLException("Batch Cuisine: " + e.getMessage());
        }

        // restaurant
        try {
            ps_r.executeBatch();
        } catch (SQLException e) {
            throw new SQLException("Batch Restaurant: " + e.getMessage());
        }

        // grade
        try {
            ps_g.executeBatch();
        } catch (SQLException e) {
            throw new SQLException("Batch Grade: " + e.getMessage());
        }

        // rest-cuisine
        try {
            ps_rc.executeBatch();
        } catch (SQLException e) {
            throw new SQLException("Batch Rest-Cuisine: " + e.getMessage());
        }

        // rest-grade
        try {
            ps_rg.executeBatch();
        } catch (SQLException e) {
            throw new SQLException("Batch Rest-Grade: " + e.getMessage());
        }

        count = 0;
    }

    /**
     * Function to close any open things for proper termination of this thread
     * @throws SQLException 
     */
    private void shutdown() throws SQLException {
        ps_rg.close();
        ps_rc.close();
        ps_r.close();
        ps_c.close();
        ps_g.close();
        ps_r_s.close();
        ps_c_s.close();
        ps_g_s.close();
        conn.close();
    }
}
