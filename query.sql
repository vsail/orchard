SELECT DISTINCT
       r.`Name` AS Name
     , g.`Grade`
     , MAX(g.`GradeDate`) AS Date
     , g.`Score` AS Score
     , r.`ID` AS RestID
  FROM Cuisine AS c
     , Restaurant_Cuisine AS rc
     , Restaurant AS r
     , Grade AS g
     , Restaurant_Grade AS rg
 WHERE c.`Name` = 'Thai'
   AND rc.`CuisineID` = c.`CuisineID`
   AND r.`ID` = rc.`RestaurantID`
   AND rg.`RestaurantID` = r.`ID`
   AND g.`ID` = rg.`GradeID`
   AND (g.`Grade` = 'B' OR g.`Grade` = 'A')
GROUP BY RestID
ORDER BY Score DESC
LIMIT 10