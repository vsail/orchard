CREATE TABLE IF NOT EXISTS Cuisine
(
    CuisineID   int             NOT NULL AUTO_INCREMENT,
    Name        varchar(255),
    PRIMARY KEY (CuisineID)
);

CREATE TABLE IF NOT EXISTS Grade
(
    ID          int         NOT NULL AUTO_INCREMENT,
    Score       int,
    Grade       varchar(2),
    GradeDate   DATE,
    PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS Restaurant
(
    ID          int             NOT NULL AUTO_INCREMENT,
    Name        varchar(255),
    PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS Restaurant_Cuisine
(
    RestaurantID    int,
    CuisineID       int,
    FOREIGN KEY (RestaurantID) REFERENCES Restaurant(ID),
    FOREIGN KEY (CuisineID) REFERENCES Cuisine(CuisineID)
);

CREATE TABLE IF NOT EXISTS Restaurant_Grade
(
    RestaurantID    int,
    GradeID       int,
    FOREIGN KEY (RestaurantID) REFERENCES Restaurant(ID),
    FOREIGN KEY (GradeID) REFERENCES Grade(ID)
);