package transform;

import java.util.Date;

public class Grade {

    private int score;
    private String grade;
    private Date date;

    public Grade(int s, String g, Date date2) {
        setScore(s);
        setGrade(g);
        setDate(date2);
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
