package main;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import transform.Entry;

public class Main {

    private static final Logger _log = LogManager.getLogger();

    private static final int MAX_LOADERS = 30;
    private static final int MAX_TRANSFORMERS = 10;
    private static final int MAX_BATCH_SIZE = 100;

    public static void main(String[] args) {
        _log.info("Starting main");

        LinkedBlockingQueue<String> transferString = new LinkedBlockingQueue<String>();
        LinkedBlockingQueue<Entry> transferEntry = new LinkedBlockingQueue<Entry>();

        LinkedList<Thread> threadList = new LinkedList<Thread>();
        LinkedList<Thread> loaders = new LinkedList<Thread>();
        LinkedList<Thread> transformers = new LinkedList<Thread>();

        // start loader
        boolean success = true;
        _log.info("Starting loaders");
        for (int i = 0 ; i < MAX_LOADERS ; i++) {
            Loader load = new Loader(transferEntry, MAX_BATCH_SIZE);
            Thread loadThread = null;
            if (load.isReady()) {
                loadThread = new Thread(load, "Loader_" + i);
                loadThread.start();
                loaders.add(loadThread);
            }
            else {
                success = false;
            }
        }

        if (success) {
            // start transforming thread
            _log.info("Starting transformers");
            for (int i = 0 ; i < MAX_TRANSFORMERS ; i++) {
                Transformer transform = new Transformer(transferString, transferEntry);
                Thread transformThread = new Thread(transform, "Transformer_" + i);
                transformThread.start();
                transformers.add(transformThread);
            }

            // start reading from file
            _log.info("Starting extractors");
            for (String filename : args) {
                Extractor extract = new Extractor(filename, transferString);
                Thread extractThread = new Thread(extract, filename);
                threadList.add(extractThread);
                extractThread.start();
            }
        }

        // wait for all threads to die
        _log.info("Waiting for all threads to close");
        try {
            // extractor threads
            Iterator<Thread> iterate = threadList.iterator();
            while (iterate.hasNext()) {
                iterate.next().join();
            }
            _log.info("All extractor threads closed");

            // wait for transformer to die
            iterate = transformers.iterator();
            while (iterate.hasNext()) {
                iterate.next().join();
            }
            _log.info("All transformer threads closed");

            // loader thread
            iterate = loaders.iterator();
            while (iterate.hasNext()) {
                iterate.next().join();
            }
            _log.info("All loader threads closed");

        } catch (InterruptedException e) {
            _log.error("Interruption while waiting for thread to join");
        }

        _log.info("Finished");
    }

}
