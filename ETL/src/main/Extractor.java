package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Extractor implements Runnable{

    private static final Logger _log = LogManager.getLogger();

    private LinkedBlockingQueue<String> queue = null;
    private String filename;
    private int count = 0;

    public Extractor(String fileName, LinkedBlockingQueue<String> myQueue) {
        filename = fileName;
        queue = myQueue;
    }

    public int getCount() {
        return count;
    }

    @Override
    public void run() {
        FileReader fr = null;
        BufferedReader br = null;
        String line = null;
        try {
            // start readers
            fr = new FileReader(filename);
            br = new BufferedReader(fr);

            // loop till end
            line = br.readLine();
            if (null != line) {
                line = br.readLine();   // read first restaurant line

                while (null != line) {
                    queue.offer(line);      // offer to blocking queue
                    count++;
                    line = br.readLine();   // read next
                }
            }
            else {
                _log.error("File is empty");
            }
        } catch (Exception e) {
            _log.error("Line: " + line);
            _log.error("Count: " + count);
            _log.error("Queue size = " + queue.size());
            _log.error("Error: " + e.getMessage());
        } finally {
            if (fr != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    _log.error("Failed to close br: " + e.getMessage());
                }
                try {
                    fr.close();
                } catch (IOException e) {
                    _log.error("Failed to close fr: " + e.getMessage());
                }
            }
        }
    }

}
